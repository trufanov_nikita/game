/*This source code copyrighted by Lazy Foo' Productions (2004-2020)
and may not be redistributed without written permission.*/
//Using SDL and standard IO
#include <SDL2/SDL.h>
#include <stdio.h>
SDL_Window* window = NULL;
SDL_Surface* screenSurface = NULL;
SDL_Surface* image = NULL;
void start_SDL();
void create_window();
void load_image();
void apply_image();
//Screen dimension constants
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
enum KeyPressSurfaces
{
    KEY_PRESS_SURFACE_DEFAULT,
    KEY_PRESS_SURFACE_UP,
    KEY_PRESS_SURFACE_DOWN,
    KEY_PRESS_SURFACE_LEFT,
    KEY_PRESS_SURFACE_RIGHT,
    KEY_PRESS_SURFACE_TOTAL
};
int main( int argc, char* args[] )
{
	bool quit = false;
	SDL_Event event;
	start_SDL();
	create_window();
	//Update the surface
	load_image();
	apply_image();
	SDL_UpdateWindowSurface( window );
	while(!quit){
		while (SDL_PollEvent(&event) != 0 ){
			if (event.type == SDL_QUIT){
				quit = true;
			}
			apply_image();
			SDL_UpdateWindowSurface( window );
		}
	}
	//Wait two seconds
	//Destroy window
	SDL_DestroyWindow( window );

	//Quit SDL subsystems
	SDL_Quit();

	return 0;
}
void start_SDL(){
	if(SDL_Init(SDL_INIT_VIDEO) < 0){
		printf("Can`t start SDL %s", SDL_GetError());
	}
}
void create_window(){
	window = SDL_CreateWindow("My Game", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if( window == NULL )
	{
		printf( "Window could not be created! SDL_Error: %s\n", SDL_GetError() );
	}
	else {
		screenSurface = SDL_GetWindowSurface( window );
	}
}
void load_image(){
	image = SDL_LoadBMP("hello_world.bmp");
}
void apply_image(){
	SDL_BlitSurface( image, NULL, screenSurface, NULL );
}
